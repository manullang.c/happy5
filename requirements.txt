alabaster==0.7.12
asgiref==3.4.1
Babel==2.9.0
certifi==2020.12.5
chardet==4.0.0
colorama==0.4.4
coverage==5.5
dj-database-url==0.5.0
Django==3.2.7
django-rest-framework==0.1.0
djangorestframework==3.12.4
docutils==0.16
gunicorn==20.1.0
idna==2.10
imagesize==1.2.0
Jinja2==2.11.2
MarkupSafe==1.1.1
packaging==20.8
psycopg2==2.9.1
Pygments==2.7.3
pyparsing==2.4.7
python-decouple==3.4
pytz==2020.5
requests==2.25.1
snowballstemmer==2.0.0
Sphinx==3.4.3
sphinxcontrib-applehelp==1.0.2
sphinxcontrib-devhelp==1.0.2
sphinxcontrib-htmlhelp==1.0.3
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.3
sphinxcontrib-serializinghtml==1.1.4
sqlparse==0.4.2
urllib3==1.26.2
whitenoise==5.3.0
