from rest_framework import serializers
from .api_models import SendMessageModel


class SendMessageSerializers(serializers.Serializer):
    user_from = serializers.CharField()
    user_to = serializers.CharField()
    chat_message = serializers.CharField()

    def create(self, validated_data):
        db_object = SendMessageModel()
        db_object.user_from = validated_data.get("user_from")
        db_object.user_to = validated_data.get("user_to")
        db_object.chat_message = validated_data.get("chat_message")
        db_object.init_save_data_to_database()
        return db_object


class ListAllConversationsSerializers(serializers.Serializer):
    convos = serializers.CharField()
    last_message = serializers.CharField()
    how_many_unreads = serializers.IntegerField()


class ListAllMessagesSerializers(serializers.Serializer):
    chat_id_surr = serializers.CharField()
    user_from = serializers.CharField()
    user_to = serializers.CharField()
    is_read_by_user_to = serializers.BooleanField()
    chat_message = serializers.CharField()
    chat_timestamp = serializers.DateTimeField()
