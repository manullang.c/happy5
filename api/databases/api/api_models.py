from api.databases.api.api_queryclass import APIQueryClass


class ListAllMessagesModel():
    db_objects = APIQueryClass()

    def __init__(self):
        self.chat_id_surr = None
        self.user_from = None
        self.user_to = None
        self.is_read_by_user_to = None
        self.chat_message = None
        self.chat_timestamp = None


class SendMessageModel():
    db_objects = APIQueryClass()

    def __init__(self):
        self.user_from = None
        self.user_to = None
        self.chat_message = None

    def init_save_data_to_database(self):
        return self.db_objects.send_message(self)


class ListAllConversationsModel():
    db_objects = APIQueryClass()

    def __init__(self):
        self.convos = None
