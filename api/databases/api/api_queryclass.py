from ..DBManager import DBManager


class APIQueryClass(DBManager):
    def __init__(self):
        pass

    def send_message(self, db_models):

        database_query_1 = """INSERT INTO chats_with(user_one, user_two) 
            VALUES('{0}', '{1}');
            """.format(
            db_models.user_from,
            db_models.user_to
        )

        database_query_trans = """SELECT count(*) FROM chats_with 
            WHERE(user_one='{0}' AND user_two='{1}') 
            OR (user_one='{1}' AND user_two='{0}')""".format(
            db_models.user_from,
            db_models.user_to
        )

        database_query_2 = """INSERT INTO chats (user_from,
            user_to, is_read_by_user_to, chat_message
            ) VALUES ('{0}', '{1}', 
            '{2}', '{3}'
            );""".format(
            db_models.user_from,
            db_models.user_to,
            "false",
            db_models.chat_message,
        )

        cursor = self.query_database(database_query_trans)

        if cursor.fetchone()[0] == 0:
            self.query_database(database_query_1)
        self.query_database(database_query_2)
        return

    def list_all_messages(self, user_one, user_two):
        database_query = """ 
        SELECT * FROM chats WHERE (user_from='{0}' AND user_to='{1}') 
        OR (user_from='{1}' AND user_to='{0}') 
        ORDER BY chat_timestamp DESC;
        """.format(
            user_one,
            user_two
        )

        cursor = self.query_database(database_query)
        from .api_models import ListAllMessagesModel
        list_of_db_objects = []
        for db_row in cursor.fetchall():
            db_object = ListAllMessagesModel()
            print(db_row[0])
            print(db_row[1])
            db_object.chat_id_surr = db_row[0]
            db_object.user_from = db_row[1]
            db_object.user_to = db_row[2]
            db_object.is_read_by_user_to = db_row[3]
            db_object.chat_message = db_row[4]
            db_object.chat_timestamp = db_row[5]
            list_of_db_objects.append(db_object)

        database_query_set_to_read = """
        UPDATE chats SET is_read_by_user_to='true' WHERE user_to='{0}'
        """.format(user_one)

        self.query_database(database_query_set_to_read)

        return list_of_db_objects

    def list_all_conversations(self, user_that_requested):
        database_query = """SELECT * FROM chats_with 
        WHERE (user_one='{0}') OR (user_two='{0}')""".format(
            user_that_requested
        )

        cursor = self.query_database(database_query)
        list_of_dict_rets = []
        print("list all")
        for db_row in cursor.fetchall():

            dict_ret = {}

            if db_row[0] == user_that_requested:
                user_other = db_row[1]
                dict_ret["convos"] = """{0}-{1}""".format(
                    user_that_requested,
                    user_other
                )
            else:
                user_other = db_row[0]
                dict_ret["convos"] = """{1}-{0}""".format(
                    user_other,
                    user_that_requested
                )

            database_query_count = """SELECT COUNT(*) FROM chats
            WHERE is_read_by_user_to='false' AND ((user_from='{0}' AND user_to='{1}') OR (user_from='{1}' AND user_to='{0}'));""".format(
                user_that_requested,
                user_other
            )
            database_query_last_message = """SELECT chat_message, chat_timestamp
            FROM chats ORDER BY chat_timestamp DESC limit 1;"""

            cursor_count = self.query_database(database_query_count)
            cursor_last_message = self.query_database(
                database_query_last_message)

            obtained_count = cursor_count.fetchone()[0]
            obtained_last_message = cursor_last_message.fetchone()[0]

            dict_ret["how_many_unreads"] = obtained_count
            dict_ret["last_message"] = obtained_last_message

            list_of_dict_rets.append(dict_ret)
        return list_of_dict_rets
