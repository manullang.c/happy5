# Imported Modules
from django.http import HttpResponse
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
# Imported Models
from ..databases.api.api_models import *
# Imported Serializers
from ..databases.api.api_serializers import *


@api_view(['POST'])
def send_message(request, format=None):
    serializer = SendMessageSerializers(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def list_all_messages(request, user_1, user_2, format=None):
    items = ListAllMessagesModel.db_objects.list_all_messages(user_1, user_2)
    serializer = ListAllMessagesSerializers(items, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def list_all_conversations(request, user_requested, format=None):
    items = ListAllConversationsModel.db_objects.list_all_conversations(
        user_requested)
    serializer = ListAllConversationsSerializers(items, many=True)
    return Response(serializer.data)
