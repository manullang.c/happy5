from django.urls import path

from .db_views.api_views import send_message, list_all_messages, list_all_conversations

urlpatterns = [
    path('send_message',
         send_message, name="send_message"),
    path('list_all_messages/<str:user_1>/<str:user_2>',
         list_all_messages, name="list_all_messages"),
    path('reply_message',
         send_message, name="reply_message"),
    path('list_all_conversations/<str:user_requested>',
         list_all_conversations, name="list_all_conversations")
]
