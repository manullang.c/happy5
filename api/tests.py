from django.test import TestCase
from django.db import connection
# Create your tests here.


class FunTest(TestCase):

    def test_data(self):
        with connection.cursor() as cursor:
            query1 = "SET SEARCH_PATH TO happy5_database"

            query2 = "SELECT * FROM USERS;"
            cursor.execute("CREATE SCHEMA happy5_database;")
            cursor.execute(query1)
            cursor.execute("""CREATE TABLE USERS (
                username text NOT NULL,
                CONSTRAINT pk_users PRIMARY KEY (username)
            );""")
            cursor.execute(query2)
